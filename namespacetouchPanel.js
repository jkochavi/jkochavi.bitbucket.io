var namespacetouchPanel =
[
    [ "touch_panel", "classtouchPanel_1_1touch__panel.html", "classtouchPanel_1_1touch__panel" ],
    [ "center", "namespacetouchPanel.html#a7b78cf2238862dc7700437b6a1a81707", null ],
    [ "coordinates", "namespacetouchPanel.html#a868be3c56c23d4213f71151980295841", null ],
    [ "dimensions", "namespacetouchPanel.html#ab8fc0c5d2dfc0642a23f79afdb7c343e", null ],
    [ "panel", "namespacetouchPanel.html#ad1ecc8f62b7a3c61109318c2ce9b8b08", null ],
    [ "resolution", "namespacetouchPanel.html#af9f918e6db7b37830ffc693eb2ed451a", null ],
    [ "xm", "namespacetouchPanel.html#a607cbbf88fc260637808a5fea8feb0e6", null ],
    [ "xp", "namespacetouchPanel.html#ab377fd1e27cdba199ff78b1e65445021", null ],
    [ "ym", "namespacetouchPanel.html#a3f13dcd564dcc07bb87e63187cbe9ea7", null ],
    [ "yp", "namespacetouchPanel.html#af75a1cd14b2ab5e40e97e328a3a9a9cb", null ]
];