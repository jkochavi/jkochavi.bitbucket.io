var classMotorDriver_1_1Motor =
[
    [ "__init__", "classMotorDriver_1_1Motor.html#a3b682a97ffdc59a7176395398e10a21a", null ],
    [ "off", "classMotorDriver_1_1Motor.html#afa4e09013fb201d7fb5e7f4b0f4bf34b", null ],
    [ "set_duty", "classMotorDriver_1_1Motor.html#acc7555a78d6ec04e342f20379bf67116", null ],
    [ "setParameters", "classMotorDriver_1_1Motor.html#a74a052e378b4814bd19d2f35adf9df13", null ],
    [ "bwdChan", "classMotorDriver_1_1Motor.html#ac487b18a037e35133fae4f55f486e9a1", null ],
    [ "fwdChan", "classMotorDriver_1_1Motor.html#acfe72ccad8671e928eb6ecfa3e4afe88", null ],
    [ "gain", "classMotorDriver_1_1Motor.html#a7417c58059d057de4e097b50791faa6a", null ],
    [ "Kt", "classMotorDriver_1_1Motor.html#a7e0809c72673a0f0a53e7faac73a6031", null ],
    [ "pins", "classMotorDriver_1_1Motor.html#a0f8fcfda26591ad6c7e0d15179c4c6fa", null ],
    [ "R", "classMotorDriver_1_1Motor.html#a193eb477110e3c9c15fb0899de3afe4c", null ],
    [ "tim", "classMotorDriver_1_1Motor.html#a8c98527d0fdbfed078183506a7ffaceb", null ],
    [ "Vdc", "classMotorDriver_1_1Motor.html#a018c9ebbf66ecafd6cabb2cdd7e5cba4", null ]
];