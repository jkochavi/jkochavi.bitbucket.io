var annotated_dup =
[
    [ "controller", null, [
      [ "fullState", "classcontroller_1_1fullState.html", "classcontroller_1_1fullState" ]
    ] ],
    [ "encoder", null, [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "mcp9808", "namespacemcp9808.html", [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "MotorDriver", null, [
      [ "Motor", "classMotorDriver_1_1Motor.html", "classMotorDriver_1_1Motor" ],
      [ "DVR8847", "classMotorDriver_1_1DVR8847.html", "classMotorDriver_1_1DVR8847" ]
    ] ],
    [ "touchPanel", "namespacetouchPanel.html", [
      [ "touch_panel", "classtouchPanel_1_1touch__panel.html", "classtouchPanel_1_1touch__panel" ]
    ] ]
];