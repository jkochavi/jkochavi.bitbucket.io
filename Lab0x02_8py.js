var Lab0x02_8py =
[
    [ "button_isr", "Lab0x02_8py.html#ac3ad40c2bf89f8466bfc77cfa2e3cfdc", null ],
    [ "calculateAverage", "Lab0x02_8py.html#af6d45fae3ed20ebaa67112dc2c6686c7", null ],
    [ "ovf_callback", "Lab0x02_8py.html#a18d0d10007de2e45a4673c10c6e77903", null ],
    [ "allowPress", "Lab0x02_8py.html#a88f4d128bc80129d2c2e0d3b5f78ef41", null ],
    [ "averageResponseTime", "Lab0x02_8py.html#a0d256117c2785403b1ed3363e10ad55a", null ],
    [ "endCount", "Lab0x02_8py.html#aeb9eecb9ddd92aed7fafd07149a69b8a", null ],
    [ "extint", "Lab0x02_8py.html#a43d8577801277ac2de1f4eba4cf03066", null ],
    [ "onBoardLED", "Lab0x02_8py.html#a58ffb48da8438c0bccd18ca73d85130c", null ],
    [ "period", "Lab0x02_8py.html#afea4393f574c7af57b8411fa70eda692", null ],
    [ "prescaler", "Lab0x02_8py.html#a73b684724c8252ce26ae2cd0c437c555", null ],
    [ "responseTimes", "Lab0x02_8py.html#a4072c2f81a31623b9e82c9858835520e", null ],
    [ "saveCount", "Lab0x02_8py.html#a854022de0440c9674573aef610012fae", null ],
    [ "tim2", "Lab0x02_8py.html#a3f191efb580e02d61e1bf09f22716611", null ],
    [ "timeOn", "Lab0x02_8py.html#a89aaffc022b87f6818a00980de112b27", null ],
    [ "timeToOn", "Lab0x02_8py.html#a1507efb4322e78a4d56ea8728cf5d4d6", null ]
];