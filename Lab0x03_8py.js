var Lab0x03_8py =
[
    [ "convertADCtoVoltage", "Lab0x03_8py.html#afef7adb191a75e9a17a4f6bc4defc6a0", null ],
    [ "parseData", "Lab0x03_8py.html#ab90356ea516c72b881cb58067278dee0", null ],
    [ "printInstructions", "Lab0x03_8py.html#a6de2700e37e02cd64afee4ee8b0798e3", null ],
    [ "ADCdata", "Lab0x03_8py.html#aa391c70e3b4b88d6f785959c8ccbbad6", null ],
    [ "ADCvoltage", "Lab0x03_8py.html#a8559748d0c1d4eabf5f0fe922699b29b", null ],
    [ "baud", "Lab0x03_8py.html#aaea9d9782b5a36195ac2d75cb7fb8e9b", null ],
    [ "comPort", "Lab0x03_8py.html#acda1c7c0262e7814591e35856641ceef", null ],
    [ "currentRow", "Lab0x03_8py.html#a78d5560dd629f4254cda30faa1c354d6", null ],
    [ "dataFile", "Lab0x03_8py.html#acaf443f8a3b2232e0b2a606cff0c7af8", null ],
    [ "fileName", "Lab0x03_8py.html#ad4f1429d5b487de1ac65701093f55ee6", null ],
    [ "fileNumber", "Lab0x03_8py.html#aef71925e43e94673194396b4f6930b37", null ],
    [ "newline", "Lab0x03_8py.html#a05537ac9c14fa4a3f3cc2082e4f3297c", null ],
    [ "receipt", "Lab0x03_8py.html#aca4ec0892dc44bf0e16fe563c7e1f288", null ],
    [ "ser", "Lab0x03_8py.html#a013cfed75f6e658e35c52f4df6f4cec6", null ],
    [ "state", "Lab0x03_8py.html#a2004316cf8401cc66454299d0c73cf38", null ],
    [ "timeData", "Lab0x03_8py.html#aba82be01221785ec257d5452c9c30449", null ],
    [ "val", "Lab0x03_8py.html#a0be121f14729db0dd4ba6e7ef3c2a7fb", null ],
    [ "validFile", "Lab0x03_8py.html#a005deb4ab9cf505b40bf741820be4474", null ]
];