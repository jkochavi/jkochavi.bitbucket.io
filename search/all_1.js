var searchData=
[
  ['adc_1',['adc',['../Lab4main_8py.html#a05d760dea155ecd89b2db8c5aa11fc1a',1,'Lab4main']]],
  ['adcbuff_2',['ADCbuff',['../Lab3main_8py.html#add2cdcb771a4016af10e8d1b1df91558',1,'Lab3main']]],
  ['adcdata_3',['ADCdata',['../Lab0x03_8py.html#aa391c70e3b4b88d6f785959c8ccbbad6',1,'Lab0x03']]],
  ['adcpin_4',['ADCpin',['../Lab3main_8py.html#ab68d2e6bc25c2528e0dc07d93c607316',1,'Lab3main']]],
  ['adcreader_5',['ADCreader',['../Lab3main_8py.html#a84415664438bdb94d619c20eba51e0b7',1,'Lab3main']]],
  ['address_6',['address',['../classmcp9808_1_1MCP9808.html#aa8f9fbfa9a21c7150377e1147b2634e9',1,'mcp9808::MCP9808']]],
  ['allowpress_7',['allowPress',['../Lab0x02_8py.html#a88f4d128bc80129d2c2e0d3b5f78ef41',1,'Lab0x02']]],
  ['ambienttemp_8',['ambientTemp',['../Lab0x04__Plotting_8py.html#a8e37e7638de961a616f2a4bca30d3d38',1,'Lab0x04_Plotting']]],
  ['averageresponsetime_9',['averageResponseTime',['../Lab0x02_8py.html#a0d256117c2785403b1ed3363e10ad55a',1,'Lab0x02']]]
];
