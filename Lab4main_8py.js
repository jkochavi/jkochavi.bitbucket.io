var Lab4main_8py =
[
    [ "button_isr", "Lab4main_8py.html#a221af6922a4800a7f695e027d01d9ad8", null ],
    [ "checkTime", "Lab4main_8py.html#a8195cc94aaf9d78f0e1b5dc3a2a55228", null ],
    [ "LEDcallBack", "Lab4main_8py.html#a1620a636b2b1d12911e0b8e124851e99", null ],
    [ "adc", "Lab4main_8py.html#a05d760dea155ecd89b2db8c5aa11fc1a", null ],
    [ "ambientTemp", "Lab4main_8py.html#ae6d3da5cd64d76a01a227478bbbf1907", null ],
    [ "extint", "Lab4main_8py.html#aef48eeabc6d43ac3604f9d2860d2bba9", null ],
    [ "i2c", "Lab4main_8py.html#a743cdec5b2376a629cec1b2d1501bb90", null ],
    [ "internalTemp", "Lab4main_8py.html#a9c8ace3d29ccf6e1b0a9496a98e4c4c5", null ],
    [ "min_count", "Lab4main_8py.html#a8af47782b9c16e4bbe8b168023f25126", null ],
    [ "min_time", "Lab4main_8py.html#a7f7cd33f572f270b2b211b5b303c8f08", null ],
    [ "onBoardLED", "Lab4main_8py.html#a3ecb920a4e820bee45ac34deb8d72a22", null ],
    [ "start_time", "Lab4main_8py.html#a685198ab94d02cbccbfe1197ac12eeba", null ],
    [ "state", "Lab4main_8py.html#ae83bb1a6358c0396acd3c0e2552f6893", null ],
    [ "taking_data", "Lab4main_8py.html#a7f6c4696e3af9cf4cf9b153bb92e252a", null ],
    [ "tempSensor", "Lab4main_8py.html#a7e942737807e3d40ca9b0f61df752d5e", null ],
    [ "test_duration", "Lab4main_8py.html#aa7a843cdb7cbde97495371288a39c00a", null ],
    [ "tim1", "Lab4main_8py.html#a65f128f27c833e53e0c879846b335dce", null ]
];