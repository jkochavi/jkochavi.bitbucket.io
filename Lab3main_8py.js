var Lab3main_8py =
[
    [ "button_isr", "Lab3main_8py.html#a2ab2e9234ea9dda2cb55e28f0a253710", null ],
    [ "LEDcallBack", "Lab3main_8py.html#a361320b821b18055e97eba128a932f1b", null ],
    [ "ADCbuff", "Lab3main_8py.html#add2cdcb771a4016af10e8d1b1df91558", null ],
    [ "ADCpin", "Lab3main_8py.html#ab68d2e6bc25c2528e0dc07d93c607316", null ],
    [ "ADCreader", "Lab3main_8py.html#a84415664438bdb94d619c20eba51e0b7", null ],
    [ "exportADC", "Lab3main_8py.html#aec4ec158e41a15376c8448bf5f9b721a", null ],
    [ "extint", "Lab3main_8py.html#a63493d3ef77c5b3e961f3cdb758c0e1f", null ],
    [ "frequency", "Lab3main_8py.html#a88e3625717e3717eaf53e50157c1abfb", null ],
    [ "lowerThreshold", "Lab3main_8py.html#ae24bd7b0e5b347bf82beb1b8ef18f2b4", null ],
    [ "lowRange", "Lab3main_8py.html#a1f81a77a30225cec9d3d680f87b35a1d", null ],
    [ "onBoardLED", "Lab3main_8py.html#a3f5752383a8abfeb0b2de899cbc1ed28", null ],
    [ "PCcom", "Lab3main_8py.html#af1a08563ff5033b1065ea623f0e1ad57", null ],
    [ "period", "Lab3main_8py.html#a0624f738097d3599432d708203b6415c", null ],
    [ "preBuff", "Lab3main_8py.html#a4f3f65d64470dbce8233a91813be7ede", null ],
    [ "pressed", "Lab3main_8py.html#abd28e1ebe2bf0b8a5b3dbaef66751026", null ],
    [ "state", "Lab3main_8py.html#a68b84193f63d1b4b9bc770805034c912", null ],
    [ "tim1", "Lab3main_8py.html#a53663f90d88215573be220bd2782ce9a", null ],
    [ "tim2", "Lab3main_8py.html#a094e1ef61d21c7d774da2a22f25eaf11", null ],
    [ "time", "Lab3main_8py.html#a5ab3639fa52ffd3773b5ab09b2455095", null ],
    [ "upperThreshold", "Lab3main_8py.html#a6989a20292f1c0303973833fb4f2c02c", null ],
    [ "val", "Lab3main_8py.html#aa5bc155081f6b1b2dfdb5101ec01351d", null ]
];