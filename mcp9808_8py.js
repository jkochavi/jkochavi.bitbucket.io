var mcp9808_8py =
[
    [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ],
    [ "i2c", "mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7", null ],
    [ "state", "mcp9808_8py.html#aeecaefc52c63b6de5b49071a06ba34b5", null ],
    [ "temp_C", "mcp9808_8py.html#a34825eb869c4ed7c2da4c04c10837fb0", null ],
    [ "temp_F", "mcp9808_8py.html#a31f02e8d965da216ddfdeff1cc215b08", null ],
    [ "tempSensor", "mcp9808_8py.html#ad1e2b05e495d60d8e54837232a8ab04e", null ]
];