var files_dup =
[
    [ "controller.py", "controller_8py.html", [
      [ "fullState", "classcontroller_1_1fullState.html", "classcontroller_1_1fullState" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", "encoder_8py" ],
    [ "Lab0x01.py", "Lab0x01_8py.html", "Lab0x01_8py" ],
    [ "Lab0x02.py", "Lab0x02_8py.html", "Lab0x02_8py" ],
    [ "Lab0x03.py", "Lab0x03_8py.html", "Lab0x03_8py" ],
    [ "Lab0x04_Plotting.py", "Lab0x04__Plotting_8py.html", "Lab0x04__Plotting_8py" ],
    [ "Lab0x08_test.py", "Lab0x08__test_8py.html", "Lab0x08__test_8py" ],
    [ "Lab0x09_main.py", "Lab0x09__main_8py.html", "Lab0x09__main_8py" ],
    [ "Lab3main.py", "Lab3main_8py.html", "Lab3main_8py" ],
    [ "Lab4main.py", "Lab4main_8py.html", "Lab4main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", "MotorDriver_8py" ],
    [ "touchPanel.py", "touchPanel_8py.html", "touchPanel_8py" ],
    [ "touchPanelCalibration.py", "touchPanelCalibration_8py.html", "touchPanelCalibration_8py" ]
];